<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::post('forget', 'UserController@forget');
Route::post('reset', 'UserController@reset');

Route::group(['middleware' => 'auth:api'], function(){
Route::post('loginuser', 'UserController@loginuser');
 });
//api for managergetmanager1
Route::post('addmanager','ManagerController@addmanager');
Route::get('getmanager','ManagerController@getmanager');
Route::get('getmanager1','ManagerController@getmanager1');

Route::get('getmanagerbyid/{id}','ManagerController@getmanagerbyid');
Route::get('getmanagerby_branch_id/{id}','ManagerController@getmanagerby_branch_id');
Route::put('updatemanager/{id}','ManagerController@updatemanager');
Route::delete('deletemanager/{id}','ManagerController@deletemanager');
//Api for branch
Route::post('addbranch','BranchController@addbranch');
Route::get('getbranch','BranchController@getbranch');
Route::get('getbranchbyid/{id}','BranchController@getbranchbyid');
Route::put('updatebranch/{id}','BranchController@updatebranch');
Route::delete('deletebranch/{id}','BranchController@deletebranch');
//Api for Category
Route::post('addcategory','Category@addcategory');
Route::get('getcategory/{branch_id}','Category@getcategory');
Route::get('getcategoryforadmin','Category@getcategoryforadmin');
Route::get('getcategorybyid/{id}','Category@getcategorybyid');
Route::put('updatecategory/{id}','Category@updatecategory');
Route::delete('deletecategory/{id}','Category@deletecategory');
//Api for SubCategory
Route::post('addsubcategory','SubCategory@addsubcategory');
Route::get('getsubcategory/{branch_id}','SubCategory@getsubcategory');
Route::get('getsubcategoryforadmin','SubCategory@getsubcategoryforadmin');

Route::get('getsubcategorybyid/{id}','SubCategory@getsubcategorybyid');
Route::put('updatesubcategory/{id}','SubCategory@updatesubcategory');
Route::delete('deletesubcategory/{id}','SubCategory@deletesubcategory');
Route::get('groupby','SubCategory@groupby');
Route::get('catsearch/{id}','SubCategory@catsearch');

//Api for UOM
Route::post('adduom','UOM@adduom');
Route::get('getuom/{branch_id}','UOM@getuom');
Route::get('getuomforadmin','UOM@getuomforadmin');
Route::get('getuombyid/{id}','UOM@getuombyid');
Route::put('updateuom/{id}','UOM@updateuom');
Route::delete('deleteuom/{id}','UOM@deleteuom');
// //Api for Inventory
// Route::post('addinventory','InventoryController@addinventory');
// Route::get('getinventory','InventoryController@getinventory');
// Route::get('getinventorybyid/{id}','InventoryController@getinventorybyid');
//  Route::put('updateinventory/{id}','InventoryController@updateinventory');
// Route::delete('deleteinventory/{id}','InventoryController@deleteinventory');
// Route::get('inv_search/{id}','InventoryController@inv_search');
// //Api for Inventory/product
Route::post('addinventory','ProductController@addproduct');
Route::get('getinventory','ProductController@getproduct');
Route::get('getinventorybyid/{id}','ProductController@getproductbyid');
Route::put('updateinventory/{id}','ProductController@updateproduct');
Route::delete('deleteinventory/{id}','ProductController@deleteproduct');
Route::get('inv_search/{id}','ProductController@prod_search');

//api for product
Route::post('addproduct','ProductController@addproduct');
Route::get('getproduct/{branch_id}','ProductController@getproduct');
Route::get('getproductforadmin','ProductController@getproductforadmin');

Route::get('getproductbyid/{id}','ProductController@getproductbyid');
Route::get('getproductbycat/{cat}','ProductController@getproductbycat');
Route::get('getproductbyitemname/{itemname}','ProductController@getproductbyname');
Route::put('updateproduct/{id}','ProductController@updateproduct');
Route::put('bulk_updateproduct/{id}','ProductController@bulk_updateproduct');
Route::delete('deleteproduct/{id}','ProductController@deleteproduct');
Route::get('prod_by_itemname/{itemname}','ProductController@prod_by_itemname');
Route::get('prod_search/{id}','ProductController@prod_search');
//Route::get('prod/{id}','ProductController@prod');
//api for addons
Route::post('addaddons','AddonsController@addaddons');
Route::get('getaddons','AddonsController@getaddons');
Route::get('getaddonsbyid/{id}','AddonsController@getaddonsbyid');
Route::get('getaddonsbybid/{branch_id}','AddonsController@getaddonsbybid');
Route::put('updateaddons/{id}','AddonsController@updateaddons');
Route::delete('deleteaddons/{id}','AddonsController@deleteaddons');
//api for attendance_sheet
Route::post('addattendance_sheet','AttendanceSheetController@addattendance_sheet');
Route::get('getattendance_sheet','AttendanceSheetController@getattendance_sheet');
Route::get('getattendance_sheetbyid/{id}','AttendanceSheetController@getattendance_sheetbyid');
Route::put('updateattendance_sheet/{id}','AttendanceSheetController@updateattendance_sheet');
Route::delete('deleteattendance_sheet/{id}','AttendanceSheetController@deleteattendance_sheet');
//api for Billingstaff
Route::post('addbillingstaff','BillingstaffController@addbillingstaff');
Route::get('getbillingstaff','BillingstaffController@getbillingstaff');
Route::get('getbillingstaffbyid/{id}','BillingstaffController@getbillingstaffbyid');
Route::put('updatebillingstaff/{id}','BillingstaffController@updatebillingstaff');
Route::delete('deletebillingstaff/{id}','BillingstaffController@deletebillingstaff');
//api for discount
Route::post('adddiscount','DiscountController@adddiscount');
Route::get('getdiscount/{branch_id}','DiscountController@getdiscount');
Route::get('getdiscountforadmin','DiscountController@getdiscountforadmin');

Route::get('getdiscountbyid/{id}','DiscountController@getdiscountbyid');
Route::put('updatediscount/{id}','DiscountController@updatediscount');
Route::delete('deletediscount/{id}','DiscountController@deletediscount');
//api for product_list
Route::post('adddiscountlist','DiscountlistController@adddiscountlist');
Route::get('getdiscountlist','DiscountlistController@getdiscountlist');
Route::get('getdiscountlistbyid/{id}','DiscountlistController@getdiscountlistbyid');
Route::put('updatediscountlist/{id}','DiscountlistController@updatediscountlist');
Route::delete('deletediscountlist/{id}','DiscountlistController@deletediscountlist');
//api for employee
Route::post('addemployee','EmployeeController@addemployee');
Route::get('getemployee','EmployeeController@getemployee');
Route::get('getemployeebyid/{id}','EmployeeController@getemployeebyid');
Route::put('updateemployee/{id}','EmployeeController@updateemployee');
Route::delete('deleteemployee/{id}','EmployeeController@deleteemployee');
//api for Productbranch
Route::post('addproductbranch','ProductbranchController@addproductbranch');
Route::get('getproductbranch','ProductbranchController@getproductbranch');
Route::get('getproductbranchbyid/{id}','ProductbranchController@getproductbranchbyid');
Route::put('updateproductbranch/{id}','ProductbranchController@updateproductbranch');
Route::delete('deleteproductbranch/{id}','ProductbranchController@deleteproductbranch');
//api for tax
Route::post('addtax','TaxController@addtax');
Route::get('gettax/{branch_id}','TaxController@gettax');
Route::get('gettaxforadmin','TaxController@gettaxforadmin');

Route::get('gettaxbyid/{id}','TaxController@gettaxbyid');
Route::put('updatetax/{id}','TaxController@updatetax');
Route::delete('deletetax/{id}','TaxController@deletetax');

//api for bill
Route::post('addbill','BillController@addbill');
Route::post('addbill1','BillController@addbill1');

Route::get('getbill','BillController@getbill');
Route::get('get_B_bill/{branch_id}','BillController@get_B_bill');


//api for sales report

// Route::get('sale_range/{date1}/{date2}','sales@sale_range');
Route::get('sale_A','sales@sale_A');
Route::get('sale_D','sales@sale_D');
Route::get('sale_M','sales@sale_M');
Route::get('sale_Y','sales@sale_Y');
Route::get('sale_All','sales@sale_All');
Route::get('total_bills','sales@total_bills');

Route::get('bill_items','sales@bill_items');
Route::get('item_D','sales@item_D');
Route::get('item_M','sales@item_M');
// Route::get('item_R/{date1}/{date2}','sales@item_range');
Route::get('item_R','sales@item_R');
Route::get('item_Y','sales@item_Y');
// apis for branch wise sales report generating
Route::get('b_sale_D/{branch_id}','branch_sales@b_sale_D');
Route::get('b_sale_M/{branch_id}','branch_sales@b_sale_M');
Route::get('b_sale_Y/{branch_id}','branch_sales@b_sale_Y');
Route::get('b_sale_All/{branch_id}','branch_sales@b_sale_All');
Route::get('b_sale_A/{branch_id}','branch_sales@b_sale_A');
Route::get('b_total_bills/{branch_id}','branch_sales@b_total_bills');
Route::get('b_item_D/{branch_id}','branch_sales@b_item_D');
Route::get('b_item_M/{branch_id}','branch_sales@b_item_M');
Route::get('b_item_R/{branch_id}','branch_sales@b_item_R');
Route::get('b_item_Y/{branch_id}','branch_sales@b_item_Y');

//  });
