<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BranchSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ManagerSeeder::class);
        $this->call(ProductCategory::class);
        $this->call(ProductSubCategory::class);
        $this->call(Product_UOM::class);
        $this->call(TaxSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(BillSeeder::class);
        $this->call(BillItemsSeeder::class);
        // $this->call(ProductbranchSeeder::class);
        // $this->call(AddonsSeeder::class);
        // $this->call(DiscountlistSeeder::class);
        // $this->call(DiscountSeeder::class);
    }
}
