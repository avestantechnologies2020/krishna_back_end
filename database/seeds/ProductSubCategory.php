<?php

use Illuminate\Database\Seeder;

class ProductSubCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ProductSubCategory::class, 50)->create();

    }
}
