<?php

use Illuminate\Database\Seeder;

class Product_UOM extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Product_UOM::class, 10)->create();

    }
}
