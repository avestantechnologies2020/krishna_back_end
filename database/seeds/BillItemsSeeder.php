<?php

use Illuminate\Database\Seeder;

class BillItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\bill_items::class, 500)->create();

    }
}
