<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'branch_id' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9')),
        'contact' => $faker->valid()->numerify('##########'),
        'role' => $faker->randomElement($array = array ('admin','manager')),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('Mithun@1234'), // password
        'created_at' => now(),

    ];
});
