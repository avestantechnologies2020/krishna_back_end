<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\bill;
use Faker\Generator as Faker;

$factory->define(bill::class, function (Faker $faker) {
    return [
        'branch_name' => $faker->name,
        'branch_id' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9')),
        'bill_amount' => $faker->randomElement($array = array ('4521','7845','849494','565467','451','555')),
        'tax_id' => $faker->randomElement($array = array ('1','2','3','4','5')),
        'discount_id' => $faker->randomElement($array = array ('1','2','3','4','5')),

    ];
});
