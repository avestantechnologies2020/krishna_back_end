<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\product;
use Faker\Generator as Faker;

$factory->define(product::class, function (Faker $faker) {
    return [
        'branch_id' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9')),
        'categoryname' => $faker->unique()->name,
        'subcategoryname' => $faker->unique()->name,
        'itemname' => $faker->unique()->name,
        'uomname' => $faker->unique()->name,
        'costprice' => $faker->randomElement($array = array ('50','20','44')),
        'selling_price' => $faker->randomElement($array = array ('50','20','44')),
        'quantityavl' => $faker->randomElement($array = array ('500','200','454')),
        'margin' => $faker->randomElement($array = array ('50','20','14')),
        'image' =>'https://source.unsplash.com/random',
        'created_at' => now(),
    ];
});
