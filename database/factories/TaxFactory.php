<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\tax;
use Faker\Generator as Faker;

$factory->define(tax::class, function (Faker $faker) {
    return [
        'branch_id' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9')),
        'taxname' => $faker->unique()->name,
        'percent' => $faker->randomElement($array = array ('10','20','30','40','50')),
        'created_at' => now(),
    ];
});
