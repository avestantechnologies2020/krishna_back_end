<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\branch;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(branch::class, function (Faker $faker) {
    return [
        'branch_name' => $faker->unique()->name,
        'b_GST_NO' => $faker->unique()->numerify('###-###-####'),
        'branch_area' => $faker->unique()->address,
        'branch_address' => $faker->unique()->address,

    ];
});
