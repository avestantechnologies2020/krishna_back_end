<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\bill_items;

$factory->define(bill_items::class, function (Faker $faker) {
    return [
        'branch_id' => $faker->numberBetween(1, 10),
        'bill_id' => $faker->numberBetween(1, 50),
        'item_id' => $faker->numberBetween(1, 80),
        'itemname' => $faker->name,
        'quantity' => $faker->numberBetween(200, 700),
    ];
});
