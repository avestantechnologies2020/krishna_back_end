<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\ProductCategory;

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'branch_id' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9')),
        'categoryname' => $faker->unique()->name,
        'created_at' => now(),
    ];
});
