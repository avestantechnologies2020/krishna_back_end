<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\manager;
use Faker\Generator as Faker;

$factory->define(manager::class, function (Faker $faker) {
    return [
        'branch_id' => $faker->randomElement($array = array ('1','2','3','4','5','6','7','8','9')),
        'name' => $faker->unique()->name,
        'role' => $faker->randomElement($array = array ('admin','manager')),
        'contact' => $faker->valid()->numerify('##########'),
        'email' => $faker->unique()->Email,
        'password' => 'Mithun@1234', // password
        'image' =>'https://source.unsplash.com/random',
        'created_at' => now(),
    ];
});
