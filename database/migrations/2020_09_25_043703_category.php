<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Category extends Migration
{
    public function up()
    {

        Schema::create('category', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('branch_id')->nullable()->default(0);
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->string('categoryname');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('category');
    }
}
