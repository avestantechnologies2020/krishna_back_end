<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bill_items extends Model
{
    protected $table = "bill_items";
    protected $fillable =["itemname","bill_id", "quantity"];
    // protected $casts = [
    //     'itemname' => 'array',
    //     'quantity' => 'array',
    // ];
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];
}
