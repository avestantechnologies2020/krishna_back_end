<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class discount extends Model
{
    protected $table = 'discounts';
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];

}
