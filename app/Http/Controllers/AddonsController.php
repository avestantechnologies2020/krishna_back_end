<?php

namespace App\Http\Controllers;

use App\addons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AddonsController extends Controller
{
    public function addaddons(Request $request){
        $validator = Validator::make($request->all(),[
            'branch_id' => 'required',
            'addons_name' => 'required',
            'addons_price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $addons = new addons;
        $addons->branch_id = $request->branch_id;
        $addons->addons_name = $request->addons_name;
        $addons->addons_price = $request->addons_price;
        $addons->save();
        return response()->json([
            "message" => "addons created successfully on branch id is :$addons->branch_id"
        ], 201);
    }

    // this is only use in admin dasboard when they need
    public function getaddons(){
        $data = addons::all();
        return $data;
    }

    // this is only use in admin dasboard when they need

    public function getaddonsbyid($id){
        $data = addons::find($id);
        return $data;
    }
    //     this is used for manager also admin

    public function getaddonsbybid($branch_id){
        if( !empty( $branch_id ) ) {
            $result = addons::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
    }

    public function updateaddons(Request $request, $id){
        $validator = Validator::make($request->all(),[
            'addons_name' => 'required',
            'branch_id' => 'required',
            'addons_price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $addons = addons::find($id);
        $addons->branch_id = $request->branch_id;
        $addons->addons_name = $request->addons_name;
        $addons->addons_price = $request->addons_price;
        $addons->save();

        return response()->json([
            "message" => "addons details updated"
        ], 201);
    }

    public function deleteaddons(Request $request, $id){
        $addons = addons::find($id);
        $addons->delete();

        return response()->json([
            "message" => "addons details deleted"
        ], 201);
    }
//     $user = User::where('email', '=', Input::get('email'))->first();
//     if ($user === null) {
//    // user doesn't exist
// }
}
