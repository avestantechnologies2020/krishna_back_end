<?php

namespace App\Http\Controllers;

use App\tax;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaxController extends Controller
{
    public function addtax(Request $request){
        $validator = Validator::make($request->all(), [
            'taxname' => 'required|string',
            'percent' => 'required|max:100|numeric',

        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $tax = new tax;
        $tax->branch_id = $request->branch_id;
        $tax->taxname = $request->taxname;
        $tax->percent = $request->percent;

        $tax->save();

        return response()->json([
            "message" => "tax created"
        ], 201);
    }
    public function gettax($branch_id){

        if( !empty( $branch_id ) ) {
            $result = tax::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
    }


    public function gettaxforadmin(){
        $data = tax::all();
        return $data;
    }

    public function gettaxbyid($id){
        $data = tax::find($id);
        return $data;
    }

    public function updatetax(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'taxname' => 'required',
            'percent' => 'required|max:100|numeric',

        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $tax = tax::find($id);
        $tax->taxname = $request->taxname;
        $tax->percent = $request->percent;
        $tax->save();

        return response()->json([
            "message" => "tax updated"
        ], 201);
    }

    public function deletetax($id){
        $tax = tax::find($id);
        $tax->delete();
        return response()->json([
            "message" => "tax deleted"
        ], 201);
    }

    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function index()
    // {
    //     //
    // }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     //
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     //
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  \App\tax  $tax
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show(tax $tax)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  \App\tax  $tax
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(tax $tax)
    // {
    //     //
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \App\tax  $tax
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, tax $tax)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  \App\tax  $tax
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy(tax $tax)
    // {
    //     //
    // }
}
