<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class UserController extends Controller
{
    //
    public $successStatus = 200;
    /**
         * login api
         *
         * @return \Illuminate\Http\Response
         */
        public function login(){
            if(Auth::attempt(['role' => request('role'),'email' => request('email'), 'password' => request('password')])){
                $user = Auth::user();
                $success['status'] =  "success";
                $success['name'] =  $user->name;
                //branch info
                $success['branch_id'] =  $user->branch_id;
                $success['branch_details']  = branch::where('id','=',$user->branch_id)->select('branch_name','branch_area','branch_address')->get();
                // $success['branch_area'] = branch::where('id','=',$user->branch_id)->select('branch_area')->get();
                // $success['branch_address'] = branch::where('id','=',$user->branch_id)->select('branch_address')->get();
                //
                $success['role'] =  $user->role;
                $success['token'] =  $user->createToken('MyApp')-> accessToken;

                $user->token = $user->remember_token;



                return response()->json(['data' => $success], $this-> successStatus);
            }
            else{
                return response()->json(['error'=>'Unauthorised'], 401);
            }
        }
        public function logout(){

        Auth::logout();

        }
    /**
         * Register api
         *
         * @return \Illuminate\Http\Response
         */
        public function register(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'role' => 'required',
                'branch_id' => 'required',
                'email' => 'required|email',
                // 'password' => 'required',
                'password' => [
                    'required',
                    'string',
                    'min:6',             // must be at least 6 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&]/', // must contain a special character
                ],
                'c_password' => 'required|same:password',
            ]);
            if ($validator->fails())
                {
                    return response()->json(['error'=>$validator->errors()], 401);
                }
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['branch_id'] = $user->branch_id;
            $success['name'] =  $user->name;
            $success['role'] =  $user->role;
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success'=>$success], $this-> successStatus);
        }
    /**
         * details api
         *
         * @return \Illuminate\Http\Response
         */
        public function loginuser()
        {
            $user = Auth::user();
            return response()->json(['success' => $user], $this-> successStatus);
        }

        public function forget(Request $request)
        {
            if(Auth::attempt(['role' => request('role'),'email' => request('email'),'name' => request('name'),'branch_id' => request('branch_id')]))
            {
                $input['password'] = bcrypt($request['password']);
                $user = Auth::user($input);
                return response()->json(['data' => $success], $this-> successStatus);
            }
            else{
                return response()->json(['error'=>'NO Match Found'], 304);
            }
        }

        public function reset() {
            $credentials = request()->validate([
                'email' => 'required|email',
                'token' => 'required|string',
                'password' => [
                    'required',
                    'string',
                    'min:6',             // must be at least 6 characters in length
                    'regex:/[a-z]/',      // must contain at least one lowercase letter
                    'regex:/[A-Z]/',      // must contain at least one uppercase letter
                    'regex:/[0-9]/',      // must contain at least one digit
                    'regex:/[@$!%*#?&]/', // must contain a special character
                ],
            ]);

            $reset_password_status = Password::reset($credentials, function ($user, $password) {
                $user->password = bcrypt($password['password']);
                $user->save();
            });

            if ($reset_password_status == Password::INVALID_TOKEN) {
                return response()->json(["msg" => "Invalid token provided"], 400);
            }

            return response()->json(["msg" => "Password has been successfully changed"]);
        }
}
