<?php

namespace App\Http\Controllers;

use App\discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class DiscountController extends Controller
{

    public function adddiscount(Request $request){
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required',
            'discounttype' => 'required|string',
            'discountby' => 'required|string',
            'discount' => 'required|max:100',
            'startdate' => 'required|date_format:yyyy-mm-dd|after:today',
            'enddate' => 'required|date_format:yyyy-MM-dd|after:today',
        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }

        $discount = new discount;
        $discount->branch_id = $request->branch_id;
        $discount->discounttype = $request->discounttype;
        $discount->discountby = $request->discountby;
        $discount->discount = $request->discount;
        $discount->startdate = $request->startdate;
        $discount->enddate = $request->enddate;
        $discount->save();

        return response()->json([
            "message" => "discount item created"
        ], 201);
    }
    public function getdiscount($branch_id){

        if( !empty( $branch_id ) ) {
            $result = discount::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
    }

    public function getdiscountforadmin(){
        $data = discount::all();
        return $data;
    }

    public function getdiscountbyid($id){
        $data = discount::find($id);
        return $data;
    }

    public function updatediscount(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'discounttype' => 'required|string',
            'discountby' => 'required|string',
            'discount' => 'required|max:100',
            'startdate' => 'required|date_format:yyyy-mm-dd|after:today',
            'enddate' => 'required|date_format:yyyy-MM-dd|after:today',
        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $discount = discount::find($id);
        $discount->discounttype = $request->discounttype;
        $discount->discountby = $request->discountby;
        $discount->discount = $request->discount;
        $discount->startdate = $request->startdate;
        $discount->enddate = $request->enddate;
        $discount->save();

        return response()->json([
            "message" => "discount item updated"
        ], 201);
    }

    public function deletediscount($id){
        $discount = discount::find($id);
        $discount->delete();

        return response()->json([
            "message" => "discount item deleted"
        ], 201);
    }
}
