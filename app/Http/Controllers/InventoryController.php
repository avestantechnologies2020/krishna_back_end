<?php

namespace App\Http\Controllers;

use App\inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InventoryController extends Controller
{
    public function addinventory(Request $request){
        $validator = Validator::make($request->all(), [

            'branch_name' => 'required',
            'itemname' => 'required',
            'unit' => 'required',
            'quantity' => 'required|numeric',
            'cost' => 'required|numeric',
            'selling_price' => 'required|numeric',
            'total' => 'required|numeric',
        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $inventory = new inventory;
        $inventory->branch_name = $request->branch_name;
        $inventory->itemname = $request->itemname;
        $inventory->unit = $request->unit;
        $inventory->quantity = $request->quantity;
        $inventory->cost = $request->cost;
        $inventory->selling_price = $request->selling_price;
        $inventory->total = $request->total;
        $inventory->save();

        return response()->json([
            "message" => "New inventory Created"
        ], 201);
    }

    public function getinventory(){
        $data = inventory::all();
        return $data;
    }

    public function getinventorybyid($id){
        $data = inventory::find($id);
        return $data;
    }

    public function updateinventory(Request $request, $id){

        $inventory = inventory::find($id);
        $inventory->branch_name = $request->branch_name;
        $inventory->itemname = $request->itemname;
        $inventory->unit = $request->unit;
        $inventory->quantity = $request->quantity;
        $inventory->cost = $request->cost;
        $inventory->selling_price = $request->selling_price;
        $inventory->total = $request->total;
        $inventory->save();
        return response()->json([
            "message" => "inventory updated"
        ], 201);
    }

    public function deleteinventory($id){
        $inventory = inventory::find($id);
        $inventory->delete();

        return response()->json([
            "message" => "inventory deleted"
        ], 201);
    }
    public function inv_search($q) {
        // $q = product::get('id');

         if( !empty( $q ) ) {
             $result = inventory::where('branch_name','LIKE','%'.$q.'%')
                 ->orWhere('itemname','LIKE','%'.$q.'%')
                 ->orWhere('quantity','LIKE','%'.$q.'%')
                 ->orWhere('selling_price','LIKE','%'.$q.'%')
                 ->orWhere('unit','LIKE','%'.$q.'%')
                 ->orWhere('cost','LIKE','%'.$q.'%')
                 ->get();

             if(count($result) > 0)
             {
                 return $result;
             }
             else
             {
                 return "$q No Details found. Try to search again..";
             }
         } else {
             $result = inventory::all();

             return $result;
         }//ok he
     }
}
