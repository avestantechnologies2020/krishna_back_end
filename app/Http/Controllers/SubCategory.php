<?php
//ready...mit
namespace App\Http\Controllers;

use App\ProductSubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class SubCategory extends Controller
{
    public function addsubcategory(Request $request){
        $validator = Validator::make($request->all(), [
            'categoryname' => 'required',
            'subcategoryname' => 'required',
            /* |unique:ProductSubCategory,subcategoryname'*/


        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $subcategory = new ProductSubCategory;
        $subcategory->branch_id = $request->branch_id;
        $subcategory->categoryname = $request->categoryname;
        $subcategory->subcategoryname = $request->subcategoryname;
        $subcategory->save();

        return response()->json([
            "message" => "SubCategory created"
        ], 201);
    }
    public function getsubcategory($branch_id){

        if( !empty( $branch_id ) ) {
            $result = ProductSubCategory::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
    }

    public function getsubcategoryforadmin(){
        $data = ProductSubCategory::all();
        return $data;
    }

    public function getsubcategorybyid($id){
        $data = ProductSubCategory::find($id);
        return $data;
    }

    public function updatesubcategory(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'categoryname' => 'required',
            'subcategoryname' => 'required',

        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $subcategory = ProductSubCategory::find($id);
        $subcategory->categoryname = $request->categoryname;
        $subcategory->subcategoryname = $request->subcategoryname;
        $subcategory->save();

        return response()->json([
            "message" => "SubCategory updated"
        ], 201);
    }

    public function deletesubcategory($id){
        $subcategory = ProductSubCategory::find($id);
        $subcategory->delete();

        return response()->json([
            "message" => "SubCategory deleted"
        ], 201);
    }



    public function groupby(){

        $data = DB::table('subcategory')->get();
        $attrs = [];
        foreach ($data as $key => $value) {
        // -> as it return std object
        $attrs[$value->categoryname]  [] = $value->subcategoryname;
        }

        dd($attrs);
    }
    // for study perpose
    // public function groupby(){
    // $subcategory = SubCategory::all();
    // $grouped = $subcategory->groupBy('categoryname');
    // $subcategory->groupBy(function($element){
    //     return str_replace(['-',' '],'', $element['categoryname']);
    //     });
    //     return $grouped;
    //     return response()->json([
    //     "message" => "groupby performed"
    //     ], 201);

    public function catsearch($q)
    {

         if( !empty( $q ) ) {
             $result = ProductSubCategory::where('categoryname','LIKE',$q)
             ->orWhere('subcategoryname','LIKE','%'.$q.'%')->get();

             if(count($result) > 0)
             {
                 return $result;
             }
             else
             {
                 return "No Details found. Try to search again..";
             }
            } else {
             $result = ProductSubCategory::all();

             return $result;
        }//ok he
    }
}
