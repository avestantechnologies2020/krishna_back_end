<?php

namespace App\Http\Controllers;

use App\User;
use App\branch;
use App\manager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ManagerController extends Controller
{
    public function addmanager(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'branch_id' => 'required',
            'contact' => 'required|digits:10',
            'email' => 'required',
            'password' => 'required',
            ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $manager = new manager;
        $manager->branch_id = $request->branch_id;
        $manager->name = $request->name;
        $manager->contact = $request->contact;
        $manager->role = $request->role;
        $manager->email = $request->email;
        $manager->password = $request->password;
        if($request->hasfile('image'))
            {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = 'manag'.time(). '.' .$extension;
                $file->move('uploads/managers_pics', $filename);
                $manager->image = $filename;
            }
            else
            {
                // return $request;
                $manager->image='';
            }
        if ($manager->save()) {
            $user = new User;
        $user->branch_id = $request->branch_id;
        $user->name = $request->name;
        $user->contact = $request->contact;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json([
            "message" => "manager created successfully"
        ], 201);
        }


    }

    public function getmanager()
        {
            $data = DB::table('managers')
            ->select('managers.*','branches.branch_name',)
            ->join('branches','branches.id','=','managers.branch_id')
            ->get();
            return $data;
    }
    public function getmanager1()
    {
        $data = manager::all();
        $branchList = branch::all();

        for($i = 0;$i<=count($data);$i++)
        {
            foreach ($branchList as $key => $value)
            {
                if($value->id == $data[$i]->branch_id)
                {
                  $data[$i]["branch_name"]= $value->branch_name;
                }
            }
        }
        return $data;
    }

    public function getmanagerby_branch_id($id)
           // $data =DB::select(DB::raw("SELECT branch_name  FROM branches WHERE id = $id"));
        // return $data;
    {
        $data = manager::where("branch_id", "like", "%".$id."%")->get();

        $array = array();
        for ($i = 0;$i < count($data);$i++) {

                $array[$i]["id"] = $data[$i]->id;
                $array[$i]["branch_id"] = $data[$i]->branch_id;
                $array[$i]["branch_name"] = (DB::select("SELECT branch_name  FROM branches WHERE id = $id"));
                $array[$i]["name"] = $data[$i]->name;
                $array[$i]["role"] = $data[$i]->role;
                $array[$i]["contact"] = $data[$i]->contact;
                $array[$i]["email"] = $data[$i]->email;
                $array[$i]["password"] = $data[$i]->password;
        }
        return $array;
    }

    public function getmanagerbyid($id){
        $data = manager::find($id);
        return $data;
    }

    public function updatemanager(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'branch_id' => 'required',
            'contact' => 'required|digits:10',
            'email' => 'required',
            'password' => 'required',
            ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $manager = manager::find($id);
        $manager->branch_id = $request->branch_id;
        $manager->name = $request->name;
        $manager->contact = $request->contact;
        $manager->role = $request->role;
        $manager->email = $request->email;
        $manager->password = $request->password;
        if($request->hasfile('image'))
            {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = 'MG'.time(). '.' .$extension;
                $file->move('uploads/managers_pics', $filename);
                $manager->image = $filename;
            }
            else
            {
                // return $request;
                $manager->image='';
            }
        $manager->save();

        return response()->json([
            "message" => "manager detail updated"
        ], 201);
    }

    public function deletemanager(Request $request, $id){
        $manager = manager::find($id);
        $manager->delete();

        return response()->json([
            "message" => "manager detail deleted"
        ], 201);
    }

}
