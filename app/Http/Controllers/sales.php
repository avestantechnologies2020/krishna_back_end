<?php

namespace App\Http\Controllers;

use App\bill;
use App\bill_items;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class sales extends Controller
{
    public function sale_range($date1,$date2)
    {
        $dd1 = date_create($date1);
         $d1 = date_format($dd1,'d-m-Y');
        $dd2 = date_create($date2);
         $d2 = date_format($dd2,'d-m-Y');

        $tableIds = DB::table('bills')->select(DB::raw('*'))->whereBetween('created_at', [$d1, $d2])->get();
        // return $tableIds;
             $jsonResult = array();
       for($i = 0;$i < count($tableIds);$i++)
       {
         $jsonResult[$i]["id"] = $tableIds[$i]->id;
    //    $jsonResult[$i]["name"] = $tableIds[$i]->customerid;
    //    $jsonResult[$i]["date"] = $tableIds[$i]->date;
    //    $jsonResult[$i]["vat"] = $tableIds[$i]->vat;
    //    $jsonResult[$i]["c_id"] = $tableIds[$i]->c_id;
    //    $jsonResult[$i]["remark"] = $tableIds[$i]->remark;
    //    $jsonResult[$i]["address"] = $tableIds[$i]->address;
    //    $jsonResult[$i]["cgstno"] = $tableIds[$i]->cgstno;
    //    $jsonResult[$i]["cdate"] = $tableIds[$i]->date;
    //    $jsonResult[$i]["commitdate"] = $tableIds[$i]->commitdate;
    //    $jsonResult[$i]["paytype"] = $tableIds[$i]->paytype;
    //    $jsonResult[$i]["state"] = $tableIds[$i]->c_state;
    //    $jsonResult[$i]["amount"] = $tableIds[$i]->amt;
    //    $jsonResult[$i]["amt1"] = $tableIds[$i]->amt1;
    //    $jsonResult[$i]["premarkcash"] = $tableIds[$i]->premarkcash;
    //    $jsonResult[$i]["premarkonline"] = $tableIds[$i]->premarkonline;
    //    $jsonResult[$i]["mode"] = $tableIds[$i]->mode;
    //    $jsonResult[$i]["mode1"] = $tableIds[$i]->mode1;
    //    $jsonResult[$i]["due"] = $tableIds[$i]->due;
    //    $jsonResult[$i]["total"] = $tableIds[$i]->total;
    //    $id = $tableIds[$i]->id;
    //    $jsonResult[$i]["details"] = DB::select( DB::raw("SELECT *  FROM sail_details WHERE billno = $id"));
        }
        return $jsonResult;
    }
    public function sale_A()
    {
        $saleD = bill::select(
            DB::raw("DATE_FORMAT(created_at,'%D %M') as Days"),
            DB::raw('sum(bill_amount) as Day_sale'))->orderBy('created_at','DESC')->groupBy('Days')->get();
        $saleM = bill::select(
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
            DB::raw('sum(bill_amount) as month_sale'))->orderBy('created_at','DESC')->groupBy('months')->get();
        $saleY = bill::select(
            DB::raw("DATE_FORMAT(created_at,'%Y') as year"),
            DB::raw('sum(bill_amount) as Year_sale'))->orderBy('created_at','DESC')->groupBy('year')->get();

            return response()->json(["sale_Day:" => $saleD,"sale_Mounth:" => $saleM,"sale_Year:" => $saleY], 201);

    }

    public function sale_D()
     {// ok it returns only todays total sale amo
        //for all branch sale

        $sale = bill::select(
            DB::raw("DATE_FORMAT(created_at,'%D %M') as Days"),
            DB::raw('sum(bill_amount) as Day_sale'))->orderBy('created_at','DESC')->groupBy('Days')->get();

            return $sale;

    // $bill = DB::table('bills')
    // ->select(
    //     DB::raw('DAY(created_at) as day'),
    //     DB::raw('SUM(bill_amount) as sum')
    //     )
    // ->whereDay('created_at', '=', Carbon::now()->day)
    // ->orWhereDay('created_at', '=', Carbon::now()->subMonth()->day)
    // ->groupBy('day')->get();
    // return $bill;



    //     $data = DB::table('bills')
    // ->select(
    //     DB::raw('YEAR(created_at) as year'),
    //     DB::raw('MONTH(created_at) as month'),
    //     DB::raw('SUM(bill_amount) as sum')
    //     )
    // ->groupBy('year', 'month')
    // // ->get();

    // $bill = DB::table('bills')
    // ->select(
    //     DB::raw('YEAR(created_at) as year'),
    //     DB::raw('MONTH(created_at) as month'),
    //     DB::raw('SUM(bill_amount) as sum')
    // )
    // ->whereYear('created_at', '=', Carbon::now()->year)
    // ->orWhereYear('created_at', '=', Carbon::now()->subYear()->year)
    // ->groupBy('year', 'month')
    // ->get();
    //     return $bill;



    }
    public function sale_M()
    {
                //for all branch sale

        $sale = bill::select(
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
            DB::raw('sum(bill_amount) as month_sale'))->orderBy('created_at','DESC')->groupBy('months')->get();

            return $sale;
        }
    public function sale_Y()
    {   //ok
        //sql code for getting year wise mounth wise sum of bill amount

                //for all branch sale


    // select year(created_at),month(created_at),sum(bill_amount)
    //  from bills
    //  group by year(created_at),month(created_at)
    //  order by year(created_at),month(created_at);


    ////// it gives total bill_amount this current year
    // $sale = bill::select(DB::raw('sum(bill_amount) as total_sale'),
    // DB::raw("DATE_FORMAT(created_at,'%y') as year"))->whereYear('created_at', date('Y'))->groupBy('year')->orderBy('created_at', 'ASC')->get();
    $sale = bill::select(
        DB::raw("DATE_FORMAT(created_at,'%Y') as year"),
        DB::raw('sum(bill_amount) as Year_sale'))->orderBy('created_at','DESC')->groupBy('year')->get();

        return $sale;
    }
    public function sale_All()
    {
                //for all branch sale

        //it gives total sale (total of bill_amount)
           $sale = DB::table("bills")->get()->sum("bill_amount");

            return response()->json([
            "sale:" => $sale
        ], 201);
    }
    public function total_bills()
    {
                //for all branch sale

        //total count of bills generated
            $all_items = DB::table("bills")->get()->count("id");
            return response()->json([
                "bill count:" => $all_items
            ], 201);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////


    public function bill_items()
    { //done
            $all_items[] = DB::table("bill_items")->get()->groupBy('itemname')->count("itemname");
            return $all_items;
    }
    public function item_D()
    {
///////////////////////////////////////////////////////////////////////////////
        //for all branch sale

$data = bill_items::orWhereDay('created_at', Carbon::now()->day)->select(DB::raw('itemname'),DB::raw('sum(quantity) as sum'))
->groupBy('itemname')
->orderBy('sum', 'DESC')->get();

return $data;

////////////////////////////////////////////////////////////////////////////////
        //$users = User::whereDay('created_at', date('d'))->get();

            // return $posts[] = bill_items::
            // // selectRaw('itemname')
            // MAX('quantity')->whereDate('created_at', date('d'))->get();


    //         $sale = bill_items::all();
    //         $data = DB::table('bills')
    // ->select(
    //     DB::raw('YEAR(created_at) as year'),
    //     DB::raw('MONTH(created_at) as month'),
    //     DB::raw('SUM(bill_amount) as sum')
    //     )
    //     ->groupBy('year', 'month')
    //     ->get();

    //         return $sale;
    }
    public function item_M()
    {
                //for all branch sale

        $data = bill_items::orWhereMonth('created_at', Carbon::now()->month)->select(DB::raw('itemname'),DB::raw('sum(quantity) as sum'))
        ->groupBy('itemname')
        ->orderBy('sum', 'DESC')->get();

        return $data;




        //$users = User::whereMonth('created_at', date('m'))->get();
        // $sales = DB::table('bill_items')
        //     ->selectRaw('bill_items.*, sum(bill_items.quantity) item_id')
        //     ->groupBy('bill_items.itemname','bill_items.quantity')
        //     ->orderBy('quantity')
        //     // ->take(5)
        //     ->get();

        // // $item_M = bill_items::select(DB::raw('COUNT(itemname) as count', 'quantity'))->groupBy('item_id')->orderBy('count', 'DESC')->take(5)->first();
        // return $item_M;
    }
   // public function item_R($date1,$date2)
    public function item_R()

    {   //code for try to bitwin date range

        $dd1 = date_create($date1);
        $d1 = date_format($dd1,'d-m-Y');
        $dd2 = date_create($date2);
        $d2 = date_format($dd2,'d-m-Y');

    //    $tableIds = DB::table('bills')->select(DB::raw('*'))->whereBetween('created_at', [$d1, $d2])->get();
    //    return $tableIds;
   return $some_data = bill_items::select('quantity')
            ->groupBy('itemname')
            ->orderByRaw('MAX(quantity) DESC')
            ->take(5)
            ->get();

    }
    public function item_Y()
    {
        //for all branch sale
        $data = bill_items::orWhereYear('created_at', Carbon::now()->year)->select(DB::raw('itemname'),DB::raw('sum(quantity) as total_quantity'))
        ->groupBy('itemname')
        ->orderBy('total_quantity', 'DESC')->get();

        return $data;




        //$users = User::whereYear('created_at', date('Y'))->get();

        // $data = DB::table('bill_items')
        // ->select(
        //     DB::raw('YEAR(created_at) as year'),
        //     DB::raw('MONTH(created_at) as month'),
        //     DB::raw('count(itemname) as itemnames')
        //     //  ->groupBy('quantity')
        //     //  ->orderBy('quantity','desc')
        // )
        // ->groupBy('year', 'month')
        // ->get();
        //     return $data;
    }

}

