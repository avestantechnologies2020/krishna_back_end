<?php

namespace App\Http\Controllers;

use App\branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    public function addbranch(Request $request){
        $validator = Validator::make($request->all(), [

            'branch_name' => 'required',
            'b_GST_NO' => 'required',
            'branch_area' => 'required',
            'branch_address' => 'required',
            ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $branch = new branch;
        $branch->branch_name = $request->branch_name;
        $branch->b_GST_NO = $request->b_GST_NO;
        $branch->branch_area = $request->branch_area;
        $branch->branch_address = $request->branch_address;
        $branch->save();

        return response()->json([
            "message" => "New Branch Created"
        ], 201);
    }

    public function getbranch(){
        $data = branch::all();
        return $data;
    }

    public function getbranchbyid($id){
        $data = branch::find($id);
        return $data;
    }

    public function updatebranch(Request $request, $id){
        $validator = Validator::make($request->all(), [

            'branch_name' => 'required',
            'b_GST_NO' => 'required',
            'branch_area' => 'required',
            'branch_address' => 'required',
            ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $branch = branch::find($id);
        $branch->branch_name = $request->branch_name;
        $branch->b_GST_NO = $request->b_GST_NO;
        $branch->branch_area = $request->branch_area;
        $branch->branch_address = $request->branch_address;
        $branch->save();
        return response()->json([
            "message" => "branch updated"
        ], 201);
    }

    public function deletebranch($id){
        $branch = branch::find($id);
        $branch->delete();

        return response()->json([
            "message" => "branch deleted"
        ], 201);
    }
}
