<?php
//ready..mit
namespace App\Http\Controllers;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Category extends Controller
{
    public function addcategory(Request $request){

        $validator = Validator::make($request->all(), [
            'categoryname' => 'required',
            'branch_id' => 'required',

           // 'categoryname' => 'required|unique:ProductCategory,categoryname,except,id',
            ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $category = new ProductCategory;
        $category->categoryname = $request->categoryname;
        $category->branch_id = $request->branch_id;

        //.............................
        // $category = $category->groupBy('categoryname');
        // $category->groupBy(function($element){
        // return str_replace(['-',' '],'', $element['categoryname']);
        // };
        //..............................
        $category->save();

        return response()->json([
            "message" => "Category created"
        ], 201);
    }
    public function getcategory($branch_id){

    if( !empty( $branch_id ) ) {
        $result = ProductCategory::where('branch_id','LIKE','%'.$branch_id.'%')->get();
        if(count($result) > 0)
        {
            return $result;
        }
        else
        {
            return "No Details found. Try to search again..with another branch_id :  $branch_id";
        }
    }
}
    //used by admin
    public function getcategoryforadmin(){
        $data = ProductCategory::all();
        return $data;
    }
    //used for admin
    public function getcategorybyid($id){
        $data = ProductCategory::find($id);
        return $data;
    }
    //used by branch and also admin
    public function updatecategory(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'categoryname' => 'required',
            ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $category = ProductCategory::find($id);
        $category->categoryname = $request->categoryname;
        $category->save();

        return response()->json([
            "message" => "Category updated"
        ], 201);
    }

    public function deletecategory($id){
        $category = ProductCategory::find($id);
        $category->delete();

        return response()->json([
            "message" => "Category deleted"
        ], 201);
    }
}
