<?php
//ready...mit
namespace App\Http\Controllers;
use App\Product_UOM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UOM extends Controller
{
    public function adduom(Request $request){

        $validator = Validator::make($request->all(), [
            'uomname' => 'required|string',
            'branch_id' => 'required',

        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }

        $uom = new Product_UOM;
        $uom->uomname = $request->uomname;
        $uom->branch_id = $request->branch_id;
        $uom->save();

        return response()->json([
            "message" => "uom created"
        ], 201);
    }
    public function getuom($branch_id){

        if( !empty( $branch_id ) ) {
            $result = Product_UOM::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
    }

    public function getuomforadmin(){
        $data = Product_UOM::all();
        return $data;
    }

    public function getuombyid($id){
        $data = Product_UOM::find($id);
        return $data;
    }

    public function updateuom(Request $request, $id){
        $uom = Product_UOM::find($id);
        $validator = Validator::make($request->all(), [
            'uomname' => 'required',
        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $uom->uomname = $request->uomname;
        $uom->save();

        return response()->json([
            "message" => "uom updated"
        ], 201);
    }

    public function deleteuom($id){
        $uom = Product_UOM::find($id);
        $uom->delete();

        return response()->json([
            "message" => "uom deleted"
        ], 201);
    }
}
