<?php

namespace App\Http\Controllers;
use App\bill;
use App\branch;

use App\product;
use App\bill_items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BillController extends Controller
{
    public function getbill(){
        $data = bill::all();
        $array = array();
        for ($i = 0;$i < count($data);$i++) {
            $array[$i]["bill_id"] = $data[$i]->id;
            $array[$i]["branch_id"] = $data[$i]->branch_id;
            $array[$i]["branch_name"] = $data[$i]->branch_name;
            $array[$i]["tax_id"] = $data[$i]->tax_id;
            $array[$i]["discount_id"] = $data[$i]->discount_id;
            $array[$i]["items"] = (bill_items::where('bill_id','LIKE','%'.$data[$i]->id.'%')->select('*')->get());

        }
        return $array;
    }
    public function get_B_bill($branch_id){
        $data = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
        $array = array();
        for ($i = 0;$i < count($data);$i++) {
            $array[$i]["bill_id"] = $data[$i]->id;
            $array[$i]["branch_id"] = $data[$i]->branch_id;
            $array[$i]["branch_name"] = $data[$i]->branch_name;
            $array[$i]["tax_id"] = $data[$i]->tax_id;
            $array[$i]["discount_id"] = $data[$i]->discount_id;
            $array[$i]["items"] = (bill_items::where('bill_id','LIKE','%'.$data[$i]->id.'%')-> where('branch_id','LIKE','%'.$branch_id.'%')->select('*')->get());

        }
        return $array;
    }
//add bill for older json data .............................................
//     public function addbill1(Request $request){
//         $bill = new bill;
//         $bill->branch_id = $request->branch_id;
//         $bill->branch_name = $request->branch_name;
//         $bill->bill_amount = $request->total;
//         $bill->tax_id = $request->tax_id;
//         $bill->discount_id = $request->discount_id;
//         if ($bill->save()) {
//             $id = $bill->id;
//             $rows=$request->billdetails;
//             if(is_array($rows) || is_object($rows))
//             {
//                 foreach($rows as $key=>$value)
//                 {
//                 $reserved = new bill_items;
//                 $reserved->bill_id = $bill->id;
//                 $reserved->item_id = $value ['id'];
//                 $reserved->itemname = $value ['itemname'];
//                 $reserved->quantity = $value ['quantity'];
//                if($reserved->save())
//                {
// //...........................................code for decrement  quantity available from product table ......................................................
//                 $query = DB::table('products')->where('id',$value ['id']);
//                 $result = $query->decrement('quantityavl',$value['quantity']);
//                }
//             }
//             return response()->json([
//                 "message" => "bill table created"
//             ], 201);

//             }
//         }

//     }
    // addbill  for new data shubham need.................................
    public function addbill(Request $request){
        $bill = new bill;
        $bill->bill_amount = $request->subtot;
        $bill->tax_id = $request->tax_id;
        $bill->discount_id = $request->discount_id;

//.........................code for getting login branch details from logBranch array.......................................

        $rows=$request->logBranch;// subham new name 1/5/2o21
        // if(is_array($rows) || is_object($rows))
        // {
        // foreach($rows as $key=>$value)// replace rows into branch
        {
            // return $rows['id'];

        $bid=$bill->branch_id = $rows ['id'];
        $bill->branch_name = $rows ['branch_name'];
        // }
        // }
        if ($bill->save()) {
            $id = $bill->id;
            $rows=$request->cartItem;
            if(is_array($rows) || is_object($rows))
            {
                foreach($rows as $key=>$value)
                {
                $reserved = new bill_items;
                $reserved->bill_id = $bill->id;
                $reserved->branch_id = $bid;
                $reserved->item_id = $value ['id'];
                $reserved->itemname = $value ['itemname'];
                $reserved->quantity = $value ['quantity'];
               if($reserved->save())
               {
//.....................code for decrement  quantity available from product table ......................................................
                $query = DB::table('products')->where('id',$value ['id']);
                $result = $query->decrement('quantityavl',$value['quantity']);
               }
            }
            return response()->json([
                "message" => "bill table created"
            ], 201);

            }
        }

    }

}
}
