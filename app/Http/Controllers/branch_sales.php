<?php

namespace App\Http\Controllers;

use App\bill;
use Carbon\Carbon;
use App\bill_items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class branch_sales extends Controller
{
    public function b_sale_D($branch_id)
     {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $sale = bill::where('branch_id','LIKE','%'.$branch_id.'%')->select(
                DB::raw("DATE_FORMAT(created_at,'%D %M') as Days"),
                DB::raw('sum(bill_amount) as Day_sale'))->orderBy('created_at','DESC')->groupBy('Days')->get();
                return $sale;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }


    public function b_sale_M($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $sale = bill::where('branch_id','LIKE','%'.$branch_id.'%')->select(
                DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
                DB::raw('sum(bill_amount) as month_sale'))->orderBy('created_at','DESC')->groupBy('months')->get();
                return $sale;
            }
        else
        {
            return "No Details found. Try to search again..with another branch_id :  $branch_id";
        }
}
else
{
    return "No branch id found :";
}
    }
    public function b_sale_Y($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $sale = bill::where('branch_id','LIKE','%'.$branch_id.'%')->select(
                DB::raw("DATE_FORMAT(created_at,'%Y') as year"),
                DB::raw('sum(bill_amount) as Year_sale'))->orderBy('created_at','DESC')->groupBy('year')->get();
                return $sale;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
            }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_sale_All($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $sale = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get()->sum("bill_amount");
                return response()->json(["sale:" => $sale], 201);
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_sale_A($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $saleD = bill::where('branch_id','LIKE','%'.$branch_id.'%')->select(
                    DB::raw("DATE_FORMAT(created_at,'%D %M') as Days"),
                    DB::raw('sum(bill_amount) as Day_sale'))->orderBy('created_at','DESC')->groupBy('Days')->get();


                $saleM = bill::where('branch_id','LIKE','%'.$branch_id.'%')->select(
                    DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
                    DB::raw('sum(bill_amount) as month_sale'))->orderBy('created_at','DESC')->groupBy('months')->get();


                $saleY = bill::where('branch_id','LIKE','%'.$branch_id.'%')->select(
                    DB::raw("DATE_FORMAT(created_at,'%Y') as year"),
                    DB::raw('sum(bill_amount) as Year_sale'))->orderBy('created_at','DESC')->groupBy('year')->get();


                    return response()->json(["sale_Day:" => $saleD,"sale_Mounth:" => $saleM,"sale_Year:" => $saleY], 201);
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_total_bills($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $all_items = DB::table("bills")->where('branch_id','LIKE','%'.$branch_id.'%')->get()->count("id");
                return response()->json([
                "bill count:" => $all_items
                ], 201);
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_bill_items($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $all_items[] = DB::table("bill_items")->where('branch_id','LIKE','%'.$branch_id.'%')->get()->groupBy('itemname')->count("itemname");
                return $all_items;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_item_D($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $data = bill_items::where('branch_id','LIKE','%'.$branch_id.'%')->orWhereDay('created_at', Carbon::now()->day)->select(DB::raw('itemname'),DB::raw('sum(quantity) as Quantity'))
                ->groupBy('itemname')
                ->orderBy('Quantity', 'DESC')->get();
                return $data;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_item_M($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $data = bill_items::where('branch_id','LIKE','%'.$branch_id.'%')->orWhereMonth('created_at', Carbon::now()->month)->select(DB::raw('itemname'),DB::raw('sum(quantity) as Quantity'))
                ->groupBy('itemname')
                ->orderBy('Quantity', 'DESC')->get();
                return $data;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }
    public function b_item_R($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $dd1 = date_create($date1);
                $d1 = date_format($dd1,'d-m-Y');
                $dd2 = date_create($date2);
                $d2 = date_format($dd2,'d-m-Y');
                return $some_data = bill_items::select('quantity')
                ->groupBy('itemname')
                ->orderByRaw('MAX(quantity) DESC')
                ->take(5)
                ->get();
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }

    }
    public function b_item_Y($branch_id)
    {
        if( !empty( $branch_id ) ) {
            $result = bill::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                $data = bill_items::where('branch_id','LIKE','%'.$branch_id.'%')->orWhereYear('created_at', Carbon::now()->year)->select(DB::raw('itemname'),DB::raw('sum(quantity) as total_quantity'))
                ->groupBy('itemname')
                ->orderBy('total_quantity', 'DESC')->get();
                return $data;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
        else
        {
            return "No branch id found :";
        }
    }

}

