<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function addproduct(Request $request){
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required',
            'categoryname' => 'required',
            'uomname' => 'required',
            'subcategoryname' => 'required',
            'itemname' => 'required',
            'costprice' => 'required|numeric',
            'selling_price' => 'required|numeric',
            'margin' => 'required|numeric|max:100',
            'quantityavl' => 'required|numeric',
            'image' => 'required|file|mimes:jpeg,jpg,png,gif',

        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $product = new product;
        $product->uomname = $request->uomname;
        $product->branch_id = $request->branch_id;
        $product->categoryname = $request->categoryname;
        $product->subcategoryname = $request->subcategoryname;
        $product->itemname = $request->itemname;
        $product->costprice = $request->costprice;
        $product->selling_price = $request->selling_price;
        $product->margin = $request->margin;
        $product->quantityavl = $request->quantityavl;

        if($request->hasfile('image'))
            {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = 'krishnaposserver.jkgroupmanpower.in/public/uploads/products_pic/Prod'.time(). '.' .$extension;
                $file->move('uploads/products_pic', $filename);
                $product->image = $filename;
            }
            else
            {
                // return $request;
                $product->image='';
            }
        $product->save();

        return response()->json([
            "message" => "product created"
        ], 201);
    }
    public function getproduct($branch_id){

        if( !empty( $branch_id ) ) {
            $result = product::where('branch_id','LIKE','%'.$branch_id.'%')->get();
            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..with another branch_id :  $branch_id";
            }
        }
    }

    public function getproductforadmin(){
        $product = product::all();
        return $product;
    }

    public function getproductbyid($id){
        $product = product::find($id);

        return $product;
    }
    public function getproductbyitemname($itemname) {
        if( !empty( $itemname ) ) {
            $result = product::where('itemname','LIKE','%'.$itemname.'%')->where('branch_id','LIKE','%'.$branch_id.'%')
                // ->orWhere('itemname','LIKE','%'.$q.'%')
                ->get();

            if(total($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..";
            }
        } else {
            $result = product::all();

            return $result;
        }//ok he
    }

     public function getproductbycat($cat) {
        if( !empty( $cat ) ) {
            $result = product::where('branch_id','LIKE','%'.$branch_id.'%')->where('categoryname','LIKE','%'.$cat.'%')
                // ->orWhere('itemname','LIKE','%'.$q.'%')
                ->get();

            if(count($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..";
            }
        } else {
            $result = product::all();

            return $result;
        }//ok he
    }

    public function updateproduct(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'uomname' => 'required',
            'categoryname' => 'required',
            'subcategoryname' => 'required',
            'itemname' => 'required',
            'costprice' => 'required|numeric',
            'selling_price' => 'required|numeric',
            'margin' => 'required|numeric|max:100',
            'quantityavl' => 'required|numeric',
            // 'image' => 'required|file|mimes:jpeg,jpg,png,gif',

        ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        $product = product::find($id);
        $product->uomname = $request->uomname;
        $product->categoryname = $request->categoryname;
        $product->subcategoryname = $request->subcategoryname;
        $product->itemname = $request->itemname;
        $product->costprice = $request->costprice;
        $product->selling_price = $request->selling_price;
        $product->margin = $request->margin;
        $product->quantityavl = $request->quantityavl;
        // if($request->hasfile('image'))
        // {
        //     $file = $request->file('image');
        //     $extension = $file->getClientOriginalExtension();
        //     $filename = 'Prod'.time() . '.' .$extension;
        //     $file->move('uploads/products_pic', $filename);
        //     $product->image = $filename;
        // }
        // else
        // {
        //     return $request;
        //     $product->image='';
        // }
         $product->save();
        return response()->json([
            "message" => "product updated"
        ], 201);
    }
    public function bulk_updateproduct(Request $request, $id){

        $product = product::find($id);
        $product->selling_price = $request->selling_price;
        $product->save();
        return response()->json([
            "message" => "selected product price updated"
        ], 201);
    }


    public function deleteproduct($id){
        $product = product::find($id);
        $product->delete();

        return response()->json([
            "message" => "product deleted"
        ], 201);
    }
    public function prod_by_itemname($request){
        $tableIds =  product::where("itemname","LIKE","%".$request."%")->where('branch_id','LIKE','%'.$branch_id.'%')->get();
        $result = array();
        for($i = 0;$i < total($tableIds);$i++)
        {
            $result[$i]["id"] = $tableIds[$i]->id;
            $result[$i]["branch_name"] = $tableIds[$i]->branch_name;
            $result[$i]["categoryname"] = $tableIds[$i]->categoryname;
            $result[$i]["subcategoryname"] = $tableIds[$i]->subcategoryname;
            $result[$i]["itemname"] = $tableIds[$i]->itemname;
            $result[$i]["image"] = $tableIds[$i]->image;
            $result[$i]["costprice"] = $tableIds[$i]->costprice;
            $result[$i]["selling_price"] = $tableIds[$i]->selling_price;
            $result[$i]["margin"] = $tableIds[$i]->margin;
            $result[$i]["quantityavl"] = $tableIds[$i]->quantityavl;
            $id = $tableIds[$i]->itemname;

            $result[$i]["product_by_items"] = product::select( product::raw("SELECT *  FROM products WHERE itemname = $id"));
        }
        return $result;
    }//ok

    public function prod_search($q) {
       // $q = product::get('id');

        if( !empty( $q ) ) {
            $result = product::where('branch_name','LIKE','%'.$q.'%')
                ->orWhere('itemname','LIKE','%'.$q.'%')
                ->orWhere('categoryname','LIKE','%'.$q.'%')
                ->orWhere('subcategoryname','LIKE','%'.$q.'%')
                ->orWhere('costprice','LIKE','%'.$q.'%')
                ->get();

            if(total($result) > 0)
            {
                return $result;
            }
            else
            {
                return "No Details found. Try to search again..";
            }
        } else {
            $result = product::all();

            return $result;
        }//ok he
    }
    public function prod($request)
    {
        $pro = product::where('itemname','LIKE','%'.$request.'%')->get();

    return $pro;
    }//ok

}
