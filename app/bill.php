<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Casts;


class bill extends Model
{
    protected $table = "bills";
    protected $fillable =["bill_no","branch_id","branch_name"];
    // protected $casts = [];
}
