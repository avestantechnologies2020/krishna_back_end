<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class manager extends Model
{
    protected $table = "managers";
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];


}
