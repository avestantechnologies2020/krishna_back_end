<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = "products";
    protected $fillable = ['branch_name',"categoryname","subcategoryname","itemname","costprice","selling_price","margin","quantityavl"];
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];

}
