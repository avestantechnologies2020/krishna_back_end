<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class addons extends Model
{
    protected $table = "addons";
    public $key = 'id';
    protected $fillable = ['addons_name','branch_id','addons_price'];
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];


}
