<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    protected $table = "branches";
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];

}
