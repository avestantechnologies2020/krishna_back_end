<?php
//done
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = "category";
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];

}

