<?php
//done
namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    protected $table = "subcategory";
    protected $hidden= ['created_at','updated_at','deleted_at','remember_token'];

}
